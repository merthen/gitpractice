package gitpractice;

import java.io.FileNotFoundException;
import java.util.List;

public class GitPractice {

    public static void main(String[] args) throws FileNotFoundException {
        List<Person> list = Reader.read();
        list.sort(new PersonNameComparator());
        List<Person> cityFilter = Filter.cityFilter("Chicago");
        for (Person person : cityFilter) {
            System.out.println(person.getEmail());
        }
    }
}
