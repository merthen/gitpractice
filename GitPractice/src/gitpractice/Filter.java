package gitpractice;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class Filter {

    public static List<Person> cityFilter(String city) throws FileNotFoundException {
        List<Person> read = Reader.read();
        List<Person> ret = new ArrayList<>();
        for (Person person : read) {
            if (city.equalsIgnoreCase(person.getCity())) {
                ret.add(person);
            }
        }
        return ret;
    }

}
