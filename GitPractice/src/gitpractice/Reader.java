package gitpractice;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Reader {

    public static List<Person> read() throws FileNotFoundException {
        List<Person> ret = new ArrayList<>();
        Scanner sc = new Scanner(new File("us-500.csv"));
        while (sc.hasNext()) {
            String[] splittedLine = sc.nextLine().split(",");
            String name = splittedLine[0] + " " + splittedLine[1];
            String city = splittedLine[4];
            String emalAddress = splittedLine[10];

            Person p = new Person(name, city, emalAddress);
            ret.add(p);

        }

        return ret;

    }

}
